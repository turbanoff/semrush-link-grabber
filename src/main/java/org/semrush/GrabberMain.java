package org.semrush;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * @author Andrey Turbanov
 */
public class GrabberMain {
    public static void main(String[] args) throws IOException, InterruptedException {
        long startTime = System.currentTimeMillis();
        HashSet<String> sitesToParse = new HashSet<>();
        try (InputStream is = GrabberMain.class.getResourceAsStream("/100DomainsForCrawling.txt");
             InputStreamReader r = new InputStreamReader(is, StandardCharsets.US_ASCII);
             BufferedReader reader = new BufferedReader(r))
        {
            String line;
            while ( (line = reader.readLine()) != null ) {
                if (!line.trim().equals("")) {
                    sitesToParse.add(line);
                    //break;
                }
            }
        }

        LinkedBlockingDeque<HashSet<String>> results = new LinkedBlockingDeque<>();
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(Runtime.getRuntime().availableProcessors() * 4);
        for (String site : sitesToParse) {
            SiteParserRunnable runnable = new SiteParserRunnable(site, results);
            runnable.schedule(executor);
        }

        try (PrintWriter resultsFile = new PrintWriter("results.txt")) {
            for (int i = 0; i < sitesToParse.size(); i++) {
                HashSet<String> take = results.take();
                for (String site : take) {
                    resultsFile.println(site);
                }
                resultsFile.println();
                resultsFile.println();
            }
        }
        executor.shutdown();
        long endTime = System.currentTimeMillis();
        System.out.println("Work time: " + TimeUnit.MILLISECONDS.toSeconds(endTime - startTime) / 60.0);
    }
}

