package org.semrush;

import java.util.Objects;

/**
 * @author Andrey Turbanov
 */
public class PageToParse implements Comparable<PageToParse> {
    public final String url;
    public final int timeoutMilliseconds;

    public PageToParse(String url, int timeoutMilliseconds) {
        this.url = url;
        this.timeoutMilliseconds = timeoutMilliseconds;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PageToParse that = (PageToParse) o;
        return Objects.equals(url, that.url);
    }

    @Override
    public int hashCode() {
        return Objects.hash(url);
    }

    private boolean isNotParseable() {
        String url = this.url.toLowerCase();
        return url.endsWith(".jpg") || url.endsWith(".png") || url.endsWith(".gif") || url.endsWith(".jpeg") ||
                url.endsWith(".pdf") || url.endsWith(".exe") || url.endsWith(".zip") ||
                url.endsWith(".mp3") || url.endsWith(".flv") || url.endsWith(".dmg");
    }

    @Override
    public int compareTo(PageToParse o) {
        if (isNotParseable() && !o.isNotParseable()) {
            return 1;
        }
        if (!isNotParseable() && o.isNotParseable()) {
            return -1;
        }
        int c = Integer.compare(timeoutMilliseconds, o.timeoutMilliseconds);
        if (c != 0) {
            return c;
        }
        return Integer.compare(url.length(), o.url.length());
    }
}
