package org.semrush;

import org.jsoup.Connection;
import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.UnsupportedMimeTypeException;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * @author Andrey Turbanov
 */
class SiteParserRunnable implements Runnable {
    private final static Logger log = LoggerFactory.getLogger(SiteParserRunnable.class);
    private final static int DEFAULT_READ_TIMEOUT_MILLISECONDS = 3000;

    private final TreeSet<PageToParse> queueToParse = new TreeSet<>();
    private final HashSet<String> result = new HashSet<>();
    private final HashSet<String> parsedPages = new HashSet<>(100);
    private final HashSet<String> urlsToSkip = new HashSet<>();
    private final String mainUrl;
    private final BlockingQueue<HashSet<String>> resultsQueue;
    private Future<?> future;
    private String currentUrl;

    public SiteParserRunnable(String url, BlockingQueue<HashSet<String>> resultsQueue) {
        mainUrl = url;
        this.resultsQueue = resultsQueue;
        add("http://" + mainUrl);
        result.add("http://" + mainUrl);
    }

    public synchronized void schedule(ScheduledExecutorService executor) {
        this.future = executor.scheduleAtFixedRate(this, 0, 1, TimeUnit.SECONDS);
    }

    @Override
    public synchronized void run() {
        Document doc = parseNext();
        if (doc == null) {
            return;
        }
        parsedPages.add(currentUrl);

        Elements links = doc.select("a");
        for (Element linkElement : links) {
            URL url = parse(linkElement.attr("href"), currentUrl);
            if (url == null) {
                continue;
            }
            String link = url.toString();
            if (!urlsToSkip.contains(link) && isSameSite(url)) {
                add(link);
                result.add(link);
            }
        }
        if (parsedPages.size() == 100) {
            stop();
        }
    }

    private URL parse(String link, String srcAddress) {
        int skipIndex = link.indexOf("#");
        String fixedLink;
        if (skipIndex == -1) {
            fixedLink = link;
        } else {
            fixedLink = link.substring(0, skipIndex);
        }
        URL srcUrl = parse(srcAddress);
        if (srcUrl == null) {
            return parse(fixedLink);
        }
        try {
            return new URL(srcUrl, fixedLink);
        } catch (MalformedURLException e) {
            return parse(fixedLink);
        }
    }

    private URL parse(String srcAddress) {
        try {
            return new URL(srcAddress);
        } catch (MalformedURLException e) {
            return null;
        }
    }

    private void stop() {
        future.cancel(false);
        resultsQueue.add(result);
    }

    private void add(String url) {
        queueToParse.add(new PageToParse(url, DEFAULT_READ_TIMEOUT_MILLISECONDS));
        urlsToSkip.add(url);
    }

    private void add(String url, int readTimeoutMilliseconds) {
        queueToParse.add(new PageToParse(url, readTimeoutMilliseconds));
        urlsToSkip.add(url);
    }

    private PageToParse take() {
        Iterator<PageToParse> iterator = queueToParse.iterator();
        if (iterator.hasNext()) {
            PageToParse next = iterator.next();
            iterator.remove();
            return next;
        }
        return null;
    }

    private boolean isSameSite(URL url) {
        String host = url.getHost();
        return host.equals(mainUrl) || host.endsWith("." + mainUrl);
    }

    private Document parseNext() {
        PageToParse currentUrlWithTimeout = take();
        if (currentUrlWithTimeout == null) {
            stop();
            return null;
        }
        currentUrl = currentUrlWithTimeout.url;
        log.debug("Parse {}", currentUrl);
        try {
            Connection.Response response = Jsoup.connect(currentUrl)
                    .followRedirects(true)
                    .timeout(currentUrlWithTimeout.timeoutMilliseconds)
                    .userAgent("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0")
                    .execute();
            return response.parse();
        } catch (MalformedURLException e) {
            log.warn("Skip malformed URL " + currentUrl);
            return null;
        } catch (UnsupportedMimeTypeException | UnknownHostException e) {
            log.info("Known exception " + e.getClass() + " " + e.getMessage() + " " + currentUrl);
            return null;
        } catch (HttpStatusException e) {
            log.info("Site returned code " + e.getStatusCode() + " " + currentUrl);
            return null;
        } catch (SocketTimeoutException e) {
            log.warn("Socket timeout when read from " + currentUrl);
            add(currentUrl, currentUrlWithTimeout.timeoutMilliseconds * 2);
            return null;
        } catch (IOException e) {
            log.error("IO error " + currentUrl, e);
            return null;
        }
    }
}
